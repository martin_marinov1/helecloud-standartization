﻿High Level Solution Design 









Project reference & name: 







Author:

Date:

Version:


Glossary

|**Term/acronym** **(in alphabetical order)**|**Definition**|
| :- | :- |
|||
|||
|||




1. Introduction

The purpose of this document outlines the solution X as part of Y delivery programme. The document aims to illustrate the end to end solution and consideration in relation to the business requirements.

This document would outline the scope, key design decisions and RAID items identified as part of the design discovery, and engagement with business and technical stakeholders.

The intent is that at high level design stage sufficient detail is provided across all relevant elements to be able to fix the total build and operating costs of the solution, and to be able to identify all required resources.  

2. Document Purpose
2.1. Exec Summary

After reading the Exec Summary the reader should have an initial understanding of the proposed solution and business need.

2.2. Target Audience

Identifying the target audience is essential to decide on, and then agree, the level of detail and views to be covered in the document, in that the detail must be sufficient for the target audience’s need.

Include both business and technical stakeholders.

2.3. Scope

2.3.1. In Scope

A high-level list of the key deliverables included in the design document. This list should be described in more details in the later section of this document.


2.3.2. Out of Scope

Describe items that are explicitly out of scope. Included items that have been discounted from the scope and design decisions to expand on the rationale.

2.4. Associated Documents

A list of documents reference within this design document e.g. other design documents, requirement trackability matrixes, vendor documentation etc.

|**Ref**|**Document**|**Version**|**Author**|
| :- | :- | :- | :- |
|||||
|||||
|||||
|||||



3. Requirement Verification 

***<Link to High Level Requirements/Processes>***

It assumed that all requirements are fully met by the solution, unless specifically identified in this section. Provide any requirement which is not met in Full. For any such Partial or Not Delivered requirement, state the degree to which the requirement is met, any work arounds, and reference to the appropriate CR providing Rathbones approval.

|**Requirement Reference**|**Description**|**Deviation**|**Accepted by:**|
| :- | :- | :- | :- |
|||||
|||||
|||||
|||||



4. Constraints

Sample table – where appropriate these should be referenced inline within the design section.

|**Ref**|**Description**|**When discovered**|**Implications**|
| :- | :- | :- | :- |
|C01||||
|C02||||
|C03||||


5. Design Decisions

Sample table –  where appropriate these should be referenced inline within the design section.

|` `**Ref**|**Decision**|**Date Decision Made**|**Rationale**|**Team/ Committee or Individual making decision**|**Impact of Decision**|
| :- | :- | :- | :- | :- | :- |
|DD01||||||
|DD02||||||
|DD03||||||
|DD04||||||

6. Architectural Overview
6.1. Baseline Architecture

Describe the “as is” state. Please note that where no existing solution, or integration points exist, this section may be removed. 

Include a logical diagram

(If physical view possible please also provide otherwise provide in Low level design)


6.1.1. Logical View 

(If physical view possible please also provide otherwise provide in Low level design)



6.1.2. Component Description

|**Component**|**Name**|**Function**|
| :- | :- | :- |
||||
||||
||||
||||

6.1.3. Interface Description

|**Interface**|**Source**|**Destination**|**Description**|
| :- | :- | :- | :- |
|||||
|||||
|||||

6.2. Transitional Architecture(s)

Options – as required.

Describe any interim states required as part of the migration e.g. interim upgrades required. Hardware or software services required to facilitate upgrades or migration. This will be a temporary state where the conclusion on the project delivery will be the Target Architecture.

Include a logical diagram(s)

6.2.1. Logical View

6.2.2. Component Description

|**Component**|**Name**|**Function**|
| :- | :- | :- |
||||
||||
||||
||||

6.2.3. Interface Description

|**Interface**|**Source**|**Destination**|**Description**|
| :- | :- | :- | :- |
|||||
|||||
|||||

6.3. Target Architecture

Outlines the target state solution.

Include a logical diagram

(If Physical diagram possible please also provide otherwise provide in Low level design)

The diagram should include the following:

- All environments (PROD, DR, DEV, UAT)
- Where and how new infrastructure will be located, e.g. on-premise, Rathbones Azure, suppliers cloud account, and how it will connect within existing infrastructure?
- Servers, storage, network – assume all are virtual unless there are specific technical issues that are expected to require physical resources
- Logical data flow; how does a record move through the system?
- Process flow diagram


6.3.1. Logical View

6.3.2. Component Description

|**Component**|**Name**|**Function**|
| :- | :- | :- |
||||
||||
||||
||||

6.3.3. Interface Description

|**Interface**|**Source**|**Destination**|**Description**|
| :- | :- | :- | :- |
|||||
|||||
|||||

6.4. Change Summary

Provide a summary of the changes being implemented in the context of the overall application landscape.

Include any specific considerations of impact of the proposed solution.


6.5. Delivery Approach

Provide a high-level view of how the solution should be delivered to minimise the business impact and successfully deliver the Target State.

This is not intended to be a project plan, but the guidance in this section should feed into the delivery planning.


7. Architecture Non-Compliance

The table below should be used to highlight any misalignment of the preferred solution with Approved Product List (APL), Engineering Principles (EP) or Engineering Standards (ES) arising from the preferred solution. These documents can be found [here](https://helecloud.sharepoint.com/:f:/r/Shared%20Documents/Customers/%5BR%20-%20T%5D/Rathbones/02.%20DELIVERY/%5B3273%5D%20Rathbones%20-%20MyRathbones%20team/Deliverables/InvestCloud%20Integration%202022/Compliance%20Documents?csf=1&web=1&e=gVBCsy). 

|**APL, EP or ES ref.**|**Non-conformance**|**Rationale/mitigation**|
| :- | :- | :- |
||||

8. High Level Solution Design

Architecture Domains

8.1. High Level Impact Assessment 

Provide rate the impact, strategic alignment and complexity using the guidance below and colour code accordingly.


|**Domain**|**Impact**|**Strategic Alignment**|**Complexity**|
| :- | :- | :- | :- |
|Business||||
|Application||||
|Data||||
|Technical||||
|Security||||
|Service||||

**Guidance:**

|Impact/Complexity|N/A|Low|Medium|High|
| :- | :-: | :-: | :-: | :-: |
|Strategic Alignment|N/A|Aligned|Partially|Not Aligned|

Provide a high-level summary of the impact/change of this solution on each architecture domain.  This impact may also be technical, resource, or RAID associated. 

8.2. Business Architecture 

Provide a high-level summary of business areas, capabilities and/or processes impacted.  


8.3. Application Architecture 

Provide a high-level summary of the application components referenced in the diagram above.  Any key functional considerations.  Integration approach and further interface details as well as high level environment requirements e.g. Dev, test, UAT etc.

8.4. Data Architecture

Provide a high-level summary of Data governance, GDRP or other Compliance/regulatory considerations.  Any high-level view of data flows even if at data entity level e.g. customer data.

8.5. Technical Architecture 

Provide a high-level summary of the technical components considering the infrastructure – on-prem or cloud, Networking, technical pre-reqs including client and server.

8.6. Security Architecture

Provide a high-level summary of security considerations e.g. authentication, encryption, alignment to security policies and standards etc.

8.7. Service Architecture 

Provide a high-level summary of service elements e.g. interoperability, scalability, management and support, performance, availability, and recovery.

9. Technical RAID

The following sections outline standard RAID items called out as part of the technical implementation.  Ensure these are managed with the PM in PPM tooling but extracted at the point and time of review.

9.1. Technical Risks

Sample table – where appropriate these should be referenced inline within the design section

|**Risk Number**|**Risk Identified**|**Risk Probability**|**Risk Implication**|**Attempts to mitigate/clarify**|
| :- | :- | :- | :- | :- |
|R01|||||
|R02|||||
|R03|||||
|R04|||||

9.2. Technical Assumptions

Sample table – where appropriate these should be referenced inline within the design section

|**Assumption Number**|**Assumption Description**|**Attempts to mitigate/clarify**|**Confidence Level**|**Impact of Assumption**|
| :- | :- | :- | :- | :- |
|A01|||||
|A02|||||
|A03|||||
|A04|||||

9.3. Technical Issues

Sample table – where appropriate these should be referenced inline within the design section

|**Issue Number**|**Issue Description**|**Date Issue Realised**|**Action Plan**|**Date Actions Due**|
| :- | :- | :- | :- | :- |
|I01|||||
|I02|||||
|I03|||||
|I04|||||

9.4. Technical Dependencies

Sample table – where appropriate these should be referenced inline within the design section

|<p>**Dependency**</p><p>**Number**</p>|**Dependency Description**|**Dependency Type**|**Date Dependency to be Completed**|**Impact Level**|**Mitigation Plan**|
| :- | :- | :- | :- | :- | :- |
|D01||||||
|D02||||||
|D03||||||
|D04||||||
|D05||||||
|D06||||||


10. Reviewers/Approvers

Complete the table below for the key stakeholders who will be involved in the review and approval of the design prior to TDA submission.

The following representatives should review the document prior to TDA, and feedback to the author that the document has been reviewed.

**The date of review response to be included, showing the document has been reviewed prior to TDA.**

|**Name**|**Title**|**Review/For information**|**Date of Review Response**|
| - | - | - | - |
||IT SME|Review||
||Project Manager|Review||
||Test Manager|Review||
||Portfolio/Programme Manager|For Info||
||Service Owner|Review||
||Service Delivery Lead|For Info||
||Cyber Security|Review||
||Test Analyst|Review||
||Head of |Review||
||Solution Architect (Peer Review)|Review||

The TDA approval date to be updated by the author, once full TDA approval has been granted.  The document should then be updated to a major revision number and stored in the solution repository.

Approval of the document at TDA assumes an automatic approval from the following representatives:

|**Name**|**Title**|**Approve**|**Date of TDA Approval**|
| :- | :- | :- | :- |
||Head of Strategy & Architecture TDA|Approve| |
||Head of Cyber Security|Approve| |
||Head of Service Delivery|Approve||

Note – documents that have conditional TDA approval must be completed before the Date of TDA Approval can be entered by the author, and the major revision saved in the solution repository.






11. Appendices

Included for any additional details required to support the design documented above.

